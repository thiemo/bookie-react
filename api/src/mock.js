import { USER_ROLE } from './services/authentication/roles';

const generateMockUser = (app, user, roles) => (
    app.service('users').create({
        email: `${user}@test.mail`,
        password: 'password',
        roles,
    })
);

const generateMockCategory = (app, user, title) => (
    app.service('categories').create({
        title,
        paid: 0,
        total: 0,
    }, { user })
);

const generateMockPayment = (app, user, categoryId, title, date, {
    amount, isPaid, period, from, to,
}) => (
    app.service('payments').create({
        categoryId,
        title,
        date,
        amount: amount || Math.random() * 2000,
        isPaid: isPaid === undefined ? Math.random() > 0.5 : isPaid,
        period: period || '',
        from: from || '',
        to: to || '',
    }, { user })
);

const generateMockData = async (app) => {
    const user = await generateMockUser(app, 'user', [USER_ROLE]);

    const { _id: rent } = await generateMockCategory(app, user, 'Wohnung');
    const { _id: insurances } = await generateMockCategory(app, user, 'Versicherungen');
    const { _id: media } = await generateMockCategory(app, user, 'Medien');
    const { _id: groceries } = await generateMockCategory(app, user, 'Lebensmittel');
    const { _id: takeaway } = await generateMockCategory(app, user, 'Bestellen');
    const { _id: luxury } = await generateMockCategory(app, user, 'Luxus');

    generateMockPayment(app, user, rent, 'Miete', '04.08.2018', {
        period: 1,
        from: '04.01.2018',
        amount: 400,
        isPaid: true,
    });

    generateMockPayment(app, user, rent, 'Strom', '15.08.2018', {
        period: 1,
        from: '15.01.2018',
        amount: 80,
        isPaid: true,
    });

    generateMockPayment(app, user, rent, 'Wasser', '16.08.2018', {
        period: 1,
        from: '16.01.2018',
        amount: 17,
        isPaid: true,
    });

    generateMockPayment(app, user, rent, 'Wäsche', '06.08.2018', {
        amount: 9,
        isPaid: true,
    });

    generateMockPayment(app, user, insurances, 'Krankenkasse', '15.08.2018', {
        period: 1,
        from: '15.01.2018',
        amount: 192.72,
        isPaid: true,
    });

    generateMockPayment(app, user, insurances, 'Haftpflicht', '16.07.2018', {
        period: 3,
        from: '16.01.2018',
        amount: 14.10,
        isPaid: true,
    });

    generateMockPayment(app, user, insurances, 'Fahrrad', '15.08.2018', {
        period: 1,
        from: '15.06.2018',
        amount: 12,
        isPaid: true,
    });

    generateMockPayment(app, user, insurances, 'Rente', '25.01.2018', {
        period: 12,
        from: '25.01.2018',
        amount: 60,
        isPaid: true,
    });

    generateMockPayment(app, user, media, 'Internet', '25.08.2018', {
        period: 1,
        from: '25.01.2018',
        amount: 41.99,
        isPaid: true,
    });

    generateMockPayment(app, user, media, 'Netflix', '06.08.2018', {
        period: 1,
        from: '06.01.2018',
        amount: 3,
        isPaid: true,
    });

    generateMockPayment(app, user, media, 'GEZ', '15.08.2018', {
        period: 3,
        from: '15.02.2018',
        amount: 52.50,
        isPaid: true,
    });

    generateMockPayment(app, user, media, 'Server', '16.08.2018', {
        period: 1,
        from: '16.08.2018',
        amount: 5,
        isPaid: true,
    });

    generateMockPayment(app, user, groceries, 'Aldi', '03.08.2018', {
        amount: 18.61,
        isPaid: true,
    });

    generateMockPayment(app, user, groceries, 'Penny', '04.08.2018', {
        amount: 1.47,
        isPaid: true,
    });

    generateMockPayment(app, user, groceries, 'Huel', '09.08.2018', {
        amount: 62.75,
        isPaid: true,
    });

    generateMockPayment(app, user, groceries, 'Aldi', '09.08.2018', {
        amount: 8.51,
        isPaid: true,
    });

    generateMockPayment(app, user, groceries, 'Aldi', '14.08.2018', {
        amount: 18.32,
        isPaid: true,
    });

    generateMockPayment(app, user, groceries, 'Aldi', '17.08.2018', {
        amount: 11.53,
        isPaid: false,
    });

    generateMockPayment(app, user, takeaway, 'Asiatisch', '03.08.2018', {
        amount: 8,
        isPaid: true,
    });

    generateMockPayment(app, user, luxury, 'Handy Guthaben', '06.08.2018', {
        amount: 15,
        isPaid: true,
    });

    generateMockPayment(app, user, luxury, 'WoW', '07.08.2018', {
        period: 1,
        from: '07.08.2018',
        amount: 12.99,
        isPaid: true,
    });

    generateMockPayment(app, user, luxury, 'Steam Game', '11.08.2018', {
        amount: 11.99,
        isPaid: true,
    });
};

export default generateMockData;
