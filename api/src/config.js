const maxAge = 24 * 60 * 60 * 1000; // 1 day

export default {
    authentication: {
        secret: 'super secret',
        cookie: {
            enabled: true,
            httpOnly: false,
            secure: process.env.NODE_ENV === 'production',
            maxAge,
        },
    },
};
