import { USER_ROLE } from '../services/authentication/roles';

export default function channels() {
    const app = this;

    app.on('login', (payload, { connection }) => {
        // connection can be undefined if there is no
        // real-time connection, e.g. when logging in via REST
        if (connection) {
            const { user: { _id, roles }, user } = connection;

            console.info('connection ->', user);

            if (roles.includes(USER_ROLE)) {
                app.channel(`users/${_id}`).join(connection);
            }
        }
    });

    if (process.env.NODE_ENV === 'development') {
        // app.service('users')
        //     .on('created', message => console.info('Created user ->', message))
        //     .on('updated', message => console.info('Updated user ->', message))
        //     .on('patched', message => console.info('Patched user ->', message));

        app.service('categories')
            .on('created', message => console.info('Created category ->', message))
            .on('updated', message => console.info('Updated category ->', message))
            .on('patched', message => console.info('Patched category ->', message));

        app.service('payments')
            .on('created', message => console.info('Created payment ->', message))
            .on('updated', message => console.info('Updated payment ->', message))
            .on('patched', message => console.info('Patched payment ->', message));

        app.service('payments/recurring')
            .on('created', message => console.info('Created paymentRecurring ->', message))
            .on('updated', message => console.info('Updated paymentRecurring ->', message))
            .on('patched', message => console.info('Patched paymentRecurring ->', message));

        app.service('statistics')
            .on('created', message => console.info('Created statistic ->', message))
            .on('updated', message => console.info('Updated statistic ->', message))
            .on('patched', message => console.info('Patched statistic ->', message));
    }

    // app.service('payments').publish('patched', ({ userId }) => app.channel(`users/${userId}`));
}
