import fs from 'fs';
import https from 'https';
import feathers from '@feathersjs/feathers';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';
// import morgan from 'morgan';

import config from './config';
import services from './services';
import channels from './channels';
import generateMockData from './mock';

const {
    SSLKEY,
    SSLCERT,
    APIPROTOCOL,
    APIDOMAIN,
    APIPORT,
} = process.env;

const app = express(feathers());

app
    // set app config
    .set('config', config)
    // Set up HTTP request logger
    // .use(morgan('dev'))
    // Enable CORS
    .use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    })
    // Turn on JSON body parsing for REST services
    .use(express.json())
    // Turn on URL-encoded body parsing for REST services
    .use(express.urlencoded({ extended: true }))
    // Set up REST transport using Express
    .configure(express.rest())
    // Configure the Socket.io transport
    .configure(socketio())
    // Set up services
    .configure(services)
    // set up channels
    .configure(channels)
    // Set up an error handler that gives us nicer errors
    .use(express.errorHandler());

// Start the server
let server;
if (APIPROTOCOL === 'https') {
    server = https.createServer({
        key: fs.readFileSync(SSLKEY),
        cert: fs.readFileSync(SSLCERT),
    }, app).listen(APIPORT);
    app.setup(server);
} else {
    server = app.listen(APIPORT);
}

server.on('listening', (err) => {
    if (err) {
        console.error(err);
    }
    console.info(`----\n==> 🌎  API is running on port ${APIPORT}`);
    console.info(`==> 💻  Send requests to ${APIPROTOCOL}://${APIDOMAIN}:${APIPORT}`);
});

if (process.env.NODE_ENV === 'development') {
    generateMockData(app);
}
