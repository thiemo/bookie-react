
export const isNotRole = role => (
    ({ params: { user } }) => !(user && user.roles.includes(role))
);

export default isNotRole;
