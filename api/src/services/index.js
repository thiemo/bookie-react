import auth from './authentication';
import categories from './categories';
import payments from './payments/service';
import paymentsRecurring from './paymentsRecurring';
import statistics from './statistics';
import users from './users';

export default function services() {
    const app = this;

    app.configure(auth); // must be first

    app.configure(categories);
    app.configure(payments);
    app.configure(paymentsRecurring);
    app.configure(statistics);
    app.configure(users);
}
