import { format, isFirstDayOfMonth, isLastDayOfMonth } from 'date-fns';
import { BadRequest } from '@feathersjs/errors';
import { validate } from 'feathers-hooks-common';

const serverValidation = {
    find: async (hook) => {
        const { params: { query: { date: { $gte: from, $lte: to } = {} } } } = hook;
        if (!from || !to || !isFirstDayOfMonth(from) || !isLastDayOfMonth(to)) {
            // only allow exactly one month as timeframe
            throw new BadRequest(`Invalid timeframe: ${from} - ${to}`);
        }
    },

    create: validate(async ({
        categoryId, title, date, amount, recurringId, period, from, to, ...payment
    }) => ({
        ...payment,
        categoryId,
        title,
        date: format(date, 'YYYY-MM-DD'),
        amount: Math.trunc(amount * 100),
        recurring: period ? {
            _id: recurringId,
            categoryId,
            title,
            amount: Math.trunc(amount * 100),
            period,
            from: format(from, 'YYYY-MM-DD'),
            to: to ? format(to, 'YYYY-MM-DD') : null,
        } : null,
    })),

    patch: validate(async ({
        title, date, amount, period, from, to, ...payment
    }) => ({
        ...payment,
        date: format(date, 'YYYY-MM-DD'),
        amount: Math.trunc(amount * 100),
        recurring: period ? {
            title,
            amount: Math.trunc(amount * 100),
            period,
            from: format(from, 'YYYY-MM-DD'),
            to: to ? format(to, 'YYYY-MM-DD') : null,
        } : null,
    })),
};

export default serverValidation;
