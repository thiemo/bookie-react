import JoiBase from 'joi';
import JoiExtension from 'joi-date-extensions';

const Joi = JoiBase.extend(JoiExtension);

const categoryId = Joi.string().trim().alphanum().length(16);
const title = Joi.string().trim();
const date = Joi.date().format(['DD.MM.YY', 'DD.MM.YYYY']);
const amount = Joi.number().min(0).precision(2);
const isPaid = Joi.boolean();

const recurringId = Joi.string().trim().alphanum().length(16);
const period = [Joi.number().integer().min(1).max(12), Joi.string().valid('')];
const from = Joi.when('period', {
    is: Joi.number().min(1).required(),
    then: date.required(),
});
const to = Joi.when('from', {
    is: Joi.date().required(),
    then: [date.greater(Joi.ref('from')), Joi.string().valid('')],
});

const createPayment = Joi.object().keys({
    categoryId: categoryId.required(),
    title: title.required(),
    date: date.required(),
    amount: amount.required(),
    isPaid: isPaid.required(),

    recurringId,
    period,
    from,
    to,
});

const patchPayment = Joi.object().keys({
    title: title.required(),
    date: date.required(),
    amount: amount.required(),
    isPaid: isPaid.required(),

    period,
    from,
    to,
});

export default {
    options: {
        abortEarly: false,
        convert: true,
        allowUnknown: true,
        stripUnknown: true,
    },
    create: createPayment,
    patch: patchPayment,
};
