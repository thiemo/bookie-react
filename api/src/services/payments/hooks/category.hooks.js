
export const addToCategory = async ({
    app, result: {
        _id: newId, categoryId, date, recurring,
    },
}) => {
    const { paymentIds } = await app.service('categories').get(categoryId);
    await app.service('categories').patch(categoryId, {
        paymentIds: [
            ...paymentIds,
            newId,
        ],
    }, recurring ? {
        from: recurring.from,
        to: recurring.to,
    } : {
        from: date,
        to: date,
    });
};

export const updateCategory = async ({ app, result: { categoryId, date, recurring } }) => {
    await app.service('categories').patch(categoryId, {}, recurring ? {
        from: recurring.from,
        to: recurring.to,
    } : {
        from: date,
        to: date,
    });
};

export const removeFromCategory = async ({
    app, result: {
        _id: id, categoryId, date, recurring,
    },
}) => {
    const { paymentIds } = await app.service('categories').get(categoryId);
    await app.service('categories').patch(categoryId, {
        paymentIds: paymentIds.filter(payment => payment._id !== id),
    }, recurring ? {
        from: recurring.from,
        to: recurring.to,
    } : {
        from: date,
        to: date,
    });
};
