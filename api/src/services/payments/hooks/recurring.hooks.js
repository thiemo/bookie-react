import { findEthereal, createPaymentsFromEthereal } from '../utils';

export const createRecurring = async (context) => {
    const { app, data: { recurring, ...data }, params: { user } } = context;
    let created = null;
    if (recurring) {
        if (recurring._id) {
            created = await app.service('payments/recurring').patch(recurring._id, recurring, { user });
        } else {
            created = await app.service('payments/recurring').create(recurring, { user });
        }
    }
    return {
        ...context,
        data: {
            ...data,
            recurring: created ? created._id : null,
        },
    };
};

export const patchRecurring = async (context) => {
    const {
        service, app, id, data: { recurring, ...data }, params: { user },
    } = context;
    const { categoryId, recurring: staleRecurring } = await service.get(id, { user });
    const subService = app.service('payments/recurring');

    let patched = null;
    if (recurring) {
        if (!staleRecurring) {
            patched = await subService.create({ ...recurring, categoryId }, { user });
        } else {
            patched = await subService.patch(staleRecurring._id, recurring, { user });
        }
    } else if (staleRecurring) {
        subService.remove(staleRecurring._id, { user });
    }

    return {
        ...context,
        data: {
            ...data,
            recurring: patched ? patched._id : null,
        },
    };
};

export const removeRecurring = async ({
    service, app, id, params: { user },
}) => {
    const stale = await service.get(id);
    if (stale.recurring) {
        app.service('payments/recurring').remove(stale.recurring._id, { user });
    }
};

export const findRecurring = async (context) => {
    const {
        app, result: { data }, result, params: {
            query: { categoryId, date: { $gte: from, $lte: to } },
        },
    } = context;

    const ethereal = await findEthereal(app, data, categoryId, from, to);
    const payments = createPaymentsFromEthereal(ethereal.data, from);

    return {
        ...context,
        result: {
            ...result,
            data: [
                ...data,
                ...payments,
            ],
        },
    };
};
