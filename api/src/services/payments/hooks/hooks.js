import auth from '@feathersjs/authentication';
import { restrictToOwner, associateCurrentUser } from 'feathers-authentication-hooks';
import { disallow, fastJoin } from 'feathers-hooks-common';
import validateSchema from 'feathers-hooks-validate-joi';

import serverValidation from '../validation';
import schema from '../schema';
import paymentResolvers from '../resolvers';
import { addToCategory, updateCategory, removeFromCategory } from './category.hooks';
import {
    findRecurring, createRecurring, patchRecurring, removeRecurring,
} from './recurring.hooks';

export default {
    before: {
        all: [
            auth.hooks.authenticate(['local', 'jwt']),
        ],
        find: [
            restrictToOwner(),
            serverValidation.find,
        ],
        get: [
            restrictToOwner(),
        ],
        create: [
            validateSchema.form(schema.create, schema.options),
            serverValidation.create,
            associateCurrentUser(),
            createRecurring,
        ],
        update: [
            disallow(),
        ],
        patch: [
            restrictToOwner(),
            validateSchema.form(schema.patch, schema.options),
            serverValidation.patch,
            patchRecurring,
        ],
        remove: [
            restrictToOwner(),
            removeRecurring,
        ],
    },

    after: {
        all: [
            fastJoin(paymentResolvers),
        ],
        find: [
            findRecurring,
        ],
        get: [],
        create: [
            addToCategory,
        ],
        update: [],
        patch: [
            updateCategory,
        ],
        remove: [
            removeFromCategory,
        ],
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },
};
