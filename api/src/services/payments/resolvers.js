
export default {
    joins: {
        recurring: () => async (payment, { app }) => {
            if (payment.recurring) {
                payment.recurring = (await app.service('payments/recurring').find({
                    query: { _id: payment.recurring },
                })).data[0] || null;
            }
            return payment;
        },
    },
};
