import {
    format, differenceInCalendarMonths, setDate, getDate,
} from 'date-fns';
import shortid from 'shortid';

export const findEthereal = (app, data, categoryId, from, to) => (
    app.service('payments/recurring').find({
        query: {
            _id: {
                $nin: data.filter(payment => payment.recurring)
                    .map(payment => payment.recurring._id),
            },
            categoryId,
            from: { $lte: to },
            $or: [
                { to: null },
                { to: { $gte: from } },
            ],
        },
    })
);

const _getDateInTimeframe = (first, period, from) => {
    let date = null;
    if (first >= from) {
        // 'first' can never occur after 'to' as long as the period is at least 1 month
        date = first;
    } else {
        const diff = differenceInCalendarMonths(from, first);
        if (diff % period === 0) {
            date = format(setDate(from, getDate(first)), 'YYYY-MM-DD');
        }
    }
    return date;
};

export const createPaymentsFromEthereal = (ethereal, from) => (
    ethereal.reduce((accumulator, recurring) => {
        const {
            categoryId, title, amount, from: first, period, userId,
        } = recurring;
        const date = _getDateInTimeframe(first, period, from);
        if (date) {
            accumulator.push({
                tempId: shortid.generate(),
                userId,
                categoryId,
                title,
                date,
                amount,
                isPaid: false,
                recurring,
            });
        }
        return accumulator;
    }, [])
);
