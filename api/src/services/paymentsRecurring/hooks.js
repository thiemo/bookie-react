import auth from '@feathersjs/authentication';
import { iff, isProvider, disallow } from 'feathers-hooks-common';
import { restrictToOwner, associateCurrentUser } from 'feathers-authentication-hooks';

export default {
    before: {
        all: [
            auth.hooks.authenticate(['local', 'jwt']),
        ],
        find: [
            restrictToOwner(),
            iff(
                isProvider('external'),
                disallow(),
            ),
        ],
        get: [
            restrictToOwner(),
            iff(
                isProvider('external'),
                disallow(),
            ),
        ],
        create: [
            associateCurrentUser(),
            iff(
                isProvider('external'),
                disallow(),
            ),
        ],
        update: [
            disallow(),
        ],
        patch: [
            restrictToOwner(),
            iff(
                isProvider('external'),
                disallow(),
            ),
        ],
        remove: [
            restrictToOwner(),
        ],
    },

    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },
};
