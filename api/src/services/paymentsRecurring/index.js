import feathersNedb from 'feathers-nedb';
import NeDB from 'nedb';

import hooks from './hooks';

export default function paymentsRecurringService() {
    const app = this;

    const options = {
        Model: new NeDB({
            filename: `${process.env.STORAGEDIR}/payments-recurring.nedb`,
            inMemoryOnly: process.env.NODE_ENV === 'development',
            autoload: true,
        }),
        paginate: {
            default: 100,
            max: 100,
        },
    };

    app.use('/payments/recurring', feathersNedb(options));
    app.service('/payments/recurring').hooks(hooks);
}
