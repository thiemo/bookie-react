import auth from '@feathersjs/authentication';
import local from '@feathersjs/authentication-local';
import { restrictToOwner } from 'feathers-authentication-hooks';
import { iff, isProvider, disallow } from 'feathers-hooks-common';

export default {
    before: {
        all: [
            iff(isProvider('external'), disallow()), // only server has access to users
        ],
        find: [
            auth.hooks.authenticate('jwt'),
        ],
        get: [
            auth.hooks.authenticate('jwt'),
        ],
        create: [
            local.hooks.hashPassword(),
        ],
        update: [
            local.hooks.hashPassword(),
            auth.hooks.authenticate('jwt'),
            restrictToOwner({ ownerField: '_id' }),
        ],
        patch: [
            local.hooks.hashPassword(),
            auth.hooks.authenticate('jwt'),
            restrictToOwner({ ownerField: '_id' }),
        ],
        remove: [
            auth.hooks.authenticate('jwt'),
            restrictToOwner({ ownerField: '_id' }),
        ],
    },

    after: {
        all: [local.hooks.protect('password')],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },
};
