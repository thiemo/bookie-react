import feathersNedb from 'feathers-nedb';
import NeDB from 'nedb';

import hooks from './hooks';

export default function usersService() {
    const app = this;

    const options = {
        Model: new NeDB({
            filename: `${process.env.STORAGEDIR}/users.nedb`,
            inMemoryOnly: process.env.NODE_ENV === 'development',
            autoload: true,
        }),
        paginate: {
            default: 10,
            max: 25,
        },
    };

    app.use('/users', feathersNedb(options));
    app.service('users').hooks(hooks);
}
