import { format, startOfMonth, endOfMonth } from 'date-fns';

const calculateStatisticFromPayments = payments => (
    payments.reduce(({ paid, total }, { amount, isPaid }) => ({
        paid: isPaid ? paid + amount : paid,
        total: total + amount,
    }), { paid: 0, total: 0 })
);

const calculateStatisticFromDB = async (app, categoryId, from, to) => (
    calculateStatisticFromPayments((await app.service('payments').find({
        query: {
            categoryId,
            date: {
                $gte: from,
                $lte: to,
            },
        },
    })).data)
);

export const updateStatistic = async (app, categoryId, { _id: id, date }) => {
    const statistic = await calculateStatisticFromDB(
        app, categoryId, format(startOfMonth(date), 'YYYY-MM-DD'), format(endOfMonth(date), 'YYYY-MM-DD'),
    );

    return app.service('statistics').patch(id, statistic);
};

export const createStatistic = async (app, categoryId, date, payments, user) => {
    const statistic = await calculateStatisticFromPayments(payments);
    return app.service('statistics').create({
        categoryId,
        date,
        ...statistic,
    }, { user });
};
