import { BadRequest } from '@feathersjs/errors';

export const validateDate = async (hook) => {
    const { params: { query: { date: { $gte: from, $lte: to } = {} } } } = hook;
    if (!from || !to) {
        throw new BadRequest(`Invalid timeframe: ${from} - ${to}`);
    }
};
