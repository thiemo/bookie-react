import auth from '@feathersjs/authentication';
import { restrictToOwner, associateCurrentUser } from 'feathers-authentication-hooks';
import {
    iff, isProvider, disallow, fastJoin,
} from 'feathers-hooks-common';
import { validateDate } from '../validation';
import { statisticResolver } from '../resolvers';
import { createMissingStatistics } from './statistics.hooks';

export default {
    before: {
        all: [
            auth.hooks.authenticate(['local', 'jwt']),
        ],
        find: [
            restrictToOwner(),
            iff(
                isProvider('external'),
                validateDate,
            ),
        ],
        get: [
            iff(
                isProvider('external'),
                disallow(),
            ),
            restrictToOwner(),
        ],
        create: [
            iff(
                isProvider('external'),
                disallow(),
            ),
            associateCurrentUser(),
        ],
        update: [
            disallow(),
        ],
        patch: [
            iff(
                isProvider('external'),
                disallow(),
            ),
            restrictToOwner(),
        ],
        remove: [
            iff(
                isProvider('external'),
                disallow(),
            ),
            restrictToOwner(),
        ],
    },

    after: {
        all: [],
        find: [
            iff(
                isProvider('external'),
                createMissingStatistics,
                fastJoin(statisticResolver),
            ),
        ],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },
};
