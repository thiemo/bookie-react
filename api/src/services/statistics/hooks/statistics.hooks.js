import { format, addMonths, startOfMonth } from 'date-fns';

const generateDatesInTimeframe = (from, to) => {
    const dates = [];
    let date = format(startOfMonth(from), 'YYYY-MM-DD');
    while (date <= to) {
        dates.push(date);
        date = format(addMonths(date, 1), 'YYYY-MM-DD');
    }
    return dates;
};

const createStatisticsForCategory = (
    { app, user },
    { category: { _id: categoryId }, existing, timeframe },
) => {
    const existingDates = existing.map(s => s.date);
    const missingDates = timeframe.filter(date => !existingDates.find(d => d === date));
    return Promise.all(missingDates.map(async date => (
        (await app.service('categories').get(categoryId, { resolveForMonth: date, user })).statistic
    )));
};

export const createMissingStatistics = async (context) => {
    // const { app, data: { recurring, ...data }, params: { user } } = context;
    const {
        app,
        result: { data: existing },
        result,
        params: {
            query: { date: { $gte: from, $lte: to } = {} },
            user,
        },
    } = context;

    if (!from || !to) return context;

    const timeframe = generateDatesInTimeframe(from, to);
    const categories = (await app.service('categories').find({ user })).data;

    const created = [].concat(...await Promise.all(categories.map(
        category => createStatisticsForCategory(context, {
            category,
            timeframe,
            existing: existing.filter(s => s.categoryId === category.categoryId),
        }),
    )));

    return {
        ...context,
        result: {
            ...result,
            data: [
                ...existing,
                ...created,
            ],
        },
    };
};
