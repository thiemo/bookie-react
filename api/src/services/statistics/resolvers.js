
export const statisticResolver = {
    joins: {
        title: () => async (statistic, { app, params: { user } }) => {
            statistic.title = (await app.service('categories').get(statistic.categoryId, { user })).title;
            return statistic;
        },
    },
};
