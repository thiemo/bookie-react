import { createStatistic } from '../statistics/utils';

export const paymentsResolver = {
    joins: {
        payments: () => async (category, { app, params: { from, to, user } }) => {
            if (from && to) {
                category.payments = (await app.service('payments').find({
                    query: {
                        categoryId: category._id,
                        date: {
                            $gte: from,
                            $lte: to,
                        },
                    },
                    user,
                })).data;
            }
            return category;
        },
    },
};

export const statisticResolver = {
    joins: {
        statistic: () => async (category, { app, params: { from: date, user } }) => {
            if (date) {
                const [statistic] = (await app.service('statistics').find({
                    query: { categoryId: category._id, date },
                    user,
                })).data;

                category.statistic = statistic || await createStatistic(
                    app, category._id, date, category.payments, user,
                );
            }
            return category;
        },
    },
};
