import feathersNedb from 'feathers-nedb';
import NeDB from 'nedb';

import hooks from './hooks/hooks';

export default function paymentsService() {
    const app = this;

    const options = {
        Model: new NeDB({
            filename: `${process.env.STORAGEDIR}/categories.nedb`,
            inMemoryOnly: process.env.NODE_ENV === 'development',
            autoload: true,
        }),
        paginate: {
            default: 100,
            max: 100,
        },
    };

    app.use('/categories', feathersNedb(options));
    app.service('categories').hooks(hooks);
}
