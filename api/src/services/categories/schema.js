import JoiBase from 'joi';
import JoiExtension from 'joi-date-extensions';

const Joi = JoiBase.extend(JoiExtension);

const id = Joi.string().trim().alphanum().length(16);
const title = Joi.string().trim();

const createCategory = Joi.object().keys({
    title: title.required(),
});

const patchCategory = Joi.object().keys({
    title,
    paymentIds: Joi.array().items(id),
});

export default {
    options: {
        abortEarly: false,
        convert: true,
        allowUnknown: true,
        stripUnknown: true,
    },
    create: createCategory,
    patch: patchCategory,
};
