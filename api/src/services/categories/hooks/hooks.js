import auth from '@feathersjs/authentication';
import { restrictToOwner, associateCurrentUser } from 'feathers-authentication-hooks';
import { disallow, fastJoin, paramsFromClient } from 'feathers-hooks-common';
import validateSchema from 'feathers-hooks-validate-joi';

import { validateResolveForMonth, validateCreate } from '../validation';
import schema from '../schema';
import { paymentsResolver, statisticResolver } from '../resolvers';
import { updateStatisticsInTimeframe } from './statistics.hooks';
import { removeRelated } from './payments.hooks';

export default {
    before: {
        all: [
            auth.hooks.authenticate(['local', 'jwt']),
            paramsFromClient('resolveForMonth'),
        ],
        find: [
            restrictToOwner(),
            validateResolveForMonth,
        ],
        get: [
            restrictToOwner(),
            validateResolveForMonth,
        ],
        create: [
            validateSchema.form(schema.create, schema.options),
            validateCreate,
            associateCurrentUser(),
        ],
        update: [
            disallow(),
        ],
        patch: [
            restrictToOwner(),
            validateSchema.form(schema.patch, schema.options),
        ],
        remove: [
            restrictToOwner(),
        ],
    },

    after: {
        all: [
        ],
        find: [
            fastJoin(paymentsResolver),
            fastJoin(statisticResolver),
        ],
        get: [
            fastJoin(paymentsResolver),
            fastJoin(statisticResolver),
        ],
        create: [],
        update: [],
        patch: [
            updateStatisticsInTimeframe,
        ],
        remove: [
            removeRelated,
        ],
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: [],
    },
};
