import { format, startOfMonth } from 'date-fns';

import { updateStatistic } from '../../statistics/utils';

const getChangedStatistics = async (app, categoryId, from) => {
    const date = format(startOfMonth(from), 'YYYY-MM-DD');
    return (await app.service('statistics').find({
        query: {
            categoryId,
            date: { $gte: date },
        },
    })).data;
};

export const updateStatisticsInTimeframe = async ({
    app, id: categoryId, params: { from },
}) => {
    const statistics = await getChangedStatistics(app, categoryId, from);
    await Promise.all(statistics.map(statistic => updateStatistic(app, categoryId, statistic)));
};
