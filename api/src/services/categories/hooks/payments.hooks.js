
export const removeRelated = async ({ app, id: categoryId, params: { user } }) => {
    try {
        await app.service('payments').remove(null, { query: { categoryId }, user });
    } catch (e) {
        if (e.code !== 404) throw e;
    }

    try {
        await app.service('payments/recurring').remove(null, { query: { categoryId }, user });
    } catch (e) {
        if (e.code !== 404) throw e;
    }
};
