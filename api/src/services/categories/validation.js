import { format, startOfMonth, endOfMonth } from 'date-fns';
import { validate } from 'feathers-hooks-common';

export const validateResolveForMonth = async ({ params: { resolveForMonth: date }, params }) => {
    if (date) {
        params.from = format(startOfMonth(date), 'YYYY-MM-DD');
        params.to = format(endOfMonth(date), 'YYYY-MM-DD');
    }
};

export const validateCreate = validate(async category => ({
    ...category,
    paymentIds: [],
}));
