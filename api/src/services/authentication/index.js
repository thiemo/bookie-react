import auth from '@feathersjs/authentication';
import local from '@feathersjs/authentication-local';
import jwt from '@feathersjs/authentication-jwt';

import getHooks from './hooks';

export default function authenticationService() {
    const app = this;

    const config = app.get('config').authentication;

    app
        .configure(auth(config))
        .configure(jwt())
        .configure(local());

    app.service('authentication').hooks(getHooks(config));
}
