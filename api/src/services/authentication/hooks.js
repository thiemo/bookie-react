import auth from '@feathersjs/authentication';
import { discard } from 'feathers-hooks-common';

const populateUser = authConfig => (
    async ({ app, result, ...rest }) => {
        const { userId } = await app.passport.verifyJWT(result.accessToken, authConfig);
        const user = await app.service('users').get(userId);
        return {
            ...rest,
            app,
            result: {
                ...result,
                user,
            },
        };
    }
);

export default function getHooks(config) {
    return {
        before: {
            create: [auth.hooks.authenticate(['jwt', 'local'])],
            remove: [auth.hooks.authenticate('jwt')],
        },
        after: {
            create: [
                populateUser(config),
                discard('user.password'), // local.hook.protect doesn't work with nested fields
            ],
        },
    };
}
