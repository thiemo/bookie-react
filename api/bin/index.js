#!/usr/bin/env node
if (process.env.NODE_ENV !== 'production') {
    if (
        !require('piping')({
            hook: true,
            ignore: /(\/\.|~$|\.json$)/i,
        })
    ) {
        return;
    }
}
require('./runtime.babel'); // babel registration (runtime transpilation for node)
require('../src/Api');
