import { format } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';

import DefaultLayout from 'pages/DefaultLayout';
import StatisticsList from 'components/statistics/StatisticsList';

const YearOverviewPage = ({ match }) => (
    <DefaultLayout>
        <StatisticsList date={`${match.params.year || format(new Date(), 'YYYY')}-01-01`} />
    </DefaultLayout>
);

YearOverviewPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            year: PropTypes.string,
        }).isRequired,
    }).isRequired,
};

export default YearOverviewPage;
