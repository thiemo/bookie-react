import PropTypes from 'prop-types';
import React from 'react';

import Header from 'components/Header';
import Footer from 'components/Footer';

const DefaultLayout = ({ children }) => (
    <React.Fragment>
        <Header />
        { children }
        <Footer />
    </React.Fragment>
);

DefaultLayout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default DefaultLayout;
