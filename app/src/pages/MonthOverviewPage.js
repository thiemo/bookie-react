import { format } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';

import DefaultLayout from 'pages/DefaultLayout';
import CategoryList from 'components/categories/CategoryList';

const MonthOverviewPage = ({ match }) => {
    const year = match.params.year || format(new Date(), 'YYYY');
    const month = match.params.month || format(new Date(), 'MM');
    const date = `${year}-${month}-01`;
    return (
        <DefaultLayout>
            <CategoryList date={date} />
        </DefaultLayout>
    );
};

MonthOverviewPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            year: PropTypes.string,
            month: PropTypes.string,
        }).isRequired,
    }).isRequired,
};

export default MonthOverviewPage;
