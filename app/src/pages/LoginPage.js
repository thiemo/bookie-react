import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { userSelector, UserPropType } from 'redux/modules/auth';

import LoginForm from 'components/login/LoginForm';

const LoginPage = ({ user, location: { state: { from } } }) => {
    if (user !== null) {
        return <Redirect to={from} />;
    }

    return (
        <LoginForm />
    );
};

LoginPage.propTypes = {
    user: UserPropType,
    location: PropTypes.shape({
        state: PropTypes.shape({
            from: PropTypes.shape({
                pathname: PropTypes.string.isRequired,
            }),
        }),
    }),
};

LoginPage.defaultProps = {
    user: null,
    location: { state: { from: { pathname: '/' } } },
};

export default connect(
    state => ({ user: userSelector(state) }),
)(LoginPage);
