import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, withRouter } from 'react-router-dom';

import { userSelector, UserPropType } from 'redux/modules/auth';

const PrivateRoute = ({ component: Component, user, ...rest }) => (
    <Route
        {...rest}
        render={({ location, ...props }) => (
            user !== null ? (
                <Component location={location} {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: '/login',
                        state: { from: location },
                    }}
                />
            )
        )}
    />
);

PrivateRoute.propTypes = {
    component: PropTypes.func.isRequired,
    user: UserPropType,
};

PrivateRoute.defaultProps = {
    user: null,
};

export default withRouter(connect(
    state => ({ user: userSelector(state) }),
)(PrivateRoute));
