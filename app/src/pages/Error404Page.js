import PropTypes from 'prop-types';
import React from 'react';

const style = {
    textAlign: 'center',
};

const YearOverviewPage = ({ location }) => (
    <div style={style}>
        <h3>No match for <code>{location.pathname}</code></h3>
    </div>
);

YearOverviewPage.propTypes = {
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired,
    }).isRequired,
};

export default YearOverviewPage;
