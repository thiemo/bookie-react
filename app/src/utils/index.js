
export const formatCents = amountInCents => (
    new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'EUR',
    }).format(amountInCents / 100.0)
);

export default formatCents;
