import React from 'react';
import PropTypes from 'prop-types';

const ServiceContext = React.createContext();

export const ServicesProvider = ({ api, services, children }) => (
    <ServiceContext.Provider
        value={{
            api,
            services,
        }}
    >
        {children}
    </ServiceContext.Provider>
);

ServicesProvider.propTypes = {
    api: PropTypes.shape({}).isRequired,
    services: PropTypes.shape({}).isRequired,
    children: PropTypes.node.isRequired,
};

const WithServices = WrappedComponent => (
    props => (
        <ServiceContext.Consumer>
            {
                ({ api, services }) => (
                    <WrappedComponent
                        {...props}
                        api={api}
                        services={services}
                    />
                )
            }
        </ServiceContext.Consumer>
    )
);

export default WithServices;
