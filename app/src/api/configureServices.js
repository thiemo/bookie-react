import io from 'socket.io-client';
import feathers from '@feathersjs/feathers';
import featherssocketio from '@feathersjs/socketio-client';
// import feathersrest from '@feathersjs/rest-client';
import authentication from '@feathersjs/authentication-client';
import reduxifyServices from 'feathers-redux';

export default function configureServices() {
    const {
        REACT_APP_APIPROTOCOL: protocol,
        REACT_APP_APIDOMAIN: domain,
        REACT_APP_APIPORT: port,
    } = process.env;

    // const rest = feathersrest(`${protocol}://${domain}:${port}`);
    // const api = feathers().configure(rest.fetch(window.fetch));

    const socket = io(`${protocol}://${domain}:${port}`, {
        transports: ['websocket'],
        forceNew: true,
    });
    const api = feathers()
        .configure(featherssocketio(socket))
        .configure(authentication({
            storage: window.localStorage,
        }));

    const services = reduxifyServices(api, {
        categories: 'categories',
        payments: 'payments',
        'payments/recurring': 'paymentsRecurring',
        statistics: 'statistics',
    });

    // TODO: remove socket event listener when api data is stable
    if (process.env.NODE_ENV === 'development') {
        let wasConnected = false;
        socket
            .on('connect', () => {
                if (wasConnected) {
                    window.location.reload();
                }
            })
            .on('disconnect', () => {
                wasConnected = true;
            });
    }
    // ------------- end remove -------------

    return {
        socket,
        api,
        services,
    };
}
