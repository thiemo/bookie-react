import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';

import { formatCents } from 'utils';
import WithServices from 'api/ServicesProvider';
import {
    showStatistics as showStatisticsAction,
    statisticsPerMonthSelector,
    statisticsIsLoadingSelector,
} from 'redux/modules/statistics';
import { showCategories } from 'redux/modules/categories';
import colors from 'styles/colors.styles';

import YearNavigation from 'components/nav/YearNavigation';

const borderColor = colors.secondaryMutedColor;
const style = {
    container: {
        padding: 10,
    },

    item: {

    },

    zero: {
        color: colors.secondaryMutedColor,
    },

    table: {
        borderColor,
        backgroundColor: colors.primaryAccentColor,
    },

    thead: {
        boxShadow: 'none',
        backgroundColor: colors.secondaryColor,
        color: colors.primaryColor,
    },

    tr: {
        borderColor,
        cursor: 'pointer',
    },

    th: {
        borderColor,
    },

    td: {
        borderColor,
        color: colors.secondaryAccentColor,
        textAlign: 'right',
    },
};

class StatisticsList extends React.Component {
    static propTypes = {
        statistics: PropTypes.arrayOf(PropTypes.shape({})),
        isLoading: PropTypes.bool.isRequired,
        date: PropTypes.string.isRequired,

        showStatistics: PropTypes.func.isRequired,
        onDetail: PropTypes.func.isRequired,
    };

    static defaultProps = {
        statistics: null,
    }

    componentDidMount() {
        const {
            statistics, isLoading, showStatistics, date,
        } = this.props;
        if (statistics === null && !isLoading) {
            showStatistics(format(date, 'YYYY-MM-DD'));
        }
    }

    generateColumns() {
        const { statistics: [row] } = this.props;
        const categories = Object.entries(row).filter(([key]) => !key.startsWith('_'))
            .sort(([, { title: a }], [, { title: b }]) => a.localeCompare(b))
            .map(([, statistic]) => ({
                Header: statistic.title,
                accessor: `${statistic.categoryId}.paid`,
                Cell: ({ value }) => (
                    <span style={value === 0 ? style.zero : style.item}>{formatCents(value)}</span>
                ),
            }));
        return [
            {
                Header: (
                    <FontAwesomeIcon icon="calendar-alt" />
                ),
                accessor: '_date',
                Cell: ({ value }) => format(value, 'MMMM'),
            },
            {
                Header: () => (
                    <FontAwesomeIcon icon="check" />
                ),
                accessor: '_paid',
                Cell: ({ value }) => (
                    <span style={value === 0 ? style.zero : style.item}>{formatCents(value)}</span>
                ),
            },
            {
                Header: (
                    <FontAwesomeIcon icon="times" />
                ),
                accessor: '_missing',
                Cell: ({ value }) => (
                    <span style={value === 0 ? style.zero : style.item}>{formatCents(value)}</span>
                ),
            },
            ...categories,
        ];
    }

    render() {
        const { statistics, onDetail } = this.props;
        return (
            <div style={style.container}>
                <YearNavigation />

                {statistics && statistics.length > 0 && (
                    <ReactTable
                        data={statistics}
                        columns={this.generateColumns()}
                        showPagination={false}
                        minRows={1}
                        getProps={() => ({ style: style.table })}
                        getTheadProps={() => ({ style: style.thead })}
                        getTheadThProps={() => ({ style: style.th })}
                        getTrGroupProps={(state, { original: { _date } }) => ({
                            onClick: () => onDetail(_date),
                            style: style.tr,
                        })}
                        getTdProps={() => ({ style: style.td })}
                    />
                )}
            </div>
        );
    }
}

export default WithServices(connect(
    state => ({
        statistics: statisticsPerMonthSelector(state),

        isLoading: statisticsIsLoadingSelector(state),
    }),
    (dispatch, { services }) => ({
        showStatistics: date => dispatch(showStatisticsAction(services, date)),
        onDetail: date => dispatch(showCategories(services, date)),
    }),
)(StatisticsList));
