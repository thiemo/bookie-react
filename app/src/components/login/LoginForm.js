import { withFormik } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import { resetBoxModel } from 'styles/reset.styles';
import { buttonSpace } from 'styles/button.styles';
import { alignBottomRight } from 'styles/common.styles';
import colors from 'styles/colors.styles';
import { login } from 'redux/modules/auth';

import IconSubmitButton from 'components/buttons/IconSubmitButton';
import FieldError from 'components/forms/FieldError';
import TextInput from 'components/forms/TextInput';
import PasswordInput from 'components/forms/PasswordInput';

const style = {
    centered: {
        ...resetBoxModel,
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        ...resetBoxModel,
        paddingLeft: 25,
        paddingTop: 25,
        paddingRight: 25,
        paddingBottom: 25,
        backgroundColor: colors.secondaryColor,
        borderColor: colors.primaryAccentColor,
    },
    form: {
        ...buttonSpace,
        display: 'inline-block',
    },
    submit: alignBottomRight,
};

const LoginForm = ({
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    touched,
    errors,
    isSubmitting,
    isValid,
}) => (
    <div style={style.centered}>
        <div style={style.container}>
            <form onSubmit={handleSubmit} style={style.form}>

                <FieldError
                    error={errors.general}
                    touched
                />

                <TextInput
                    label="user"
                    name="user"
                    prefix="loginform"
                    value={values.user}
                    touched={touched.user}
                    error={errors.user}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />

                <PasswordInput
                    label="password"
                    name="password"
                    prefix="loginform"
                    value={values.password}
                    touched={touched.password}
                    error={errors.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />

                <div style={style.submit}>
                    <IconSubmitButton
                        disabled={isSubmitting || !isValid}
                        icon="sign-in-alt"
                    />
                </div>
            </form>
        </div>
    </div>
);

const formFieldsPropType = (type = PropTypes.string) => PropTypes.shape({
    user: type,
    password: type,
});

LoginForm.propTypes = {
    values: formFieldsPropType().isRequired,
    errors: formFieldsPropType().isRequired,
    touched: formFieldsPropType(PropTypes.bool).isRequired,

    handleChange: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,

    isSubmitting: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
};

export default WithServices(connect(
    null,
    dispatch => ({
        onLogin: user => dispatch(login(user)),
    }),
)(withFormik({
    mapPropsToValues: () => ({
        user: '',
        password: '',
    }),
    validate: (values) => {
        const errors = {};
        if (!values.user) {
            errors.user = 'Required';
        }
        if (!values.password) {
            errors.password = 'Required';
        }
        return errors;
    },
    handleSubmit: (values, { props, setSubmitting, setErrors }) => {
        const { api, onLogin } = props;
        const { user, password } = values;

        api.authenticate({
            strategy: 'local',
            email: user, // 'user@test.mail'
            password, // 'password'
        }).then((response) => {
            setSubmitting(false);
            onLogin(response.user);
        }).catch((error) => {
            setErrors({ general: error.message });
            setSubmitting(false);
        });
    },
})(LoginForm)));
