import { format } from 'date-fns';
import { withFormik } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import { buttonSpace } from 'styles/button.styles';
import { alignBottomRight, alignBottomLeft } from 'styles/common.styles';
import { categoriesDateSelector } from 'redux/modules/categories';
import { savePayment, removePayment, PaymentPropType } from 'redux/modules/payments';

import IconSubmitButton from 'components/buttons/IconSubmitButton';
import DeleteButton from 'components/buttons/DeleteButton';
import TextInput from 'components/forms/TextInput';
// import DateInput from 'components/forms/DateInput';
import CheckboxInput from 'components/forms/CheckboxInput';

const style = {
    container: {
        position: 'relative',
    },
    form: {
        ...buttonSpace,
        display: 'inline-block',
    },
    submit: alignBottomRight,
    delete: alignBottomLeft,
};

const PaymentForm = ({
    payment,
    onDelete,
    onSuccess,
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    touched,
    errors,
    isSubmitting,
    isValid,
}) => (
    <div style={style.container}>
        <form onSubmit={handleSubmit} style={style.form}>
            <TextInput
                labelIcon="caret-right"
                name="title"
                prefix="paymentform"
                value={values.title}
                touched={touched.title}
                error={errors.title}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <TextInput
                labelIcon="calendar-alt"
                name="date"
                prefix="paymentform"
                value={values.date}
                touched={touched.date}
                error={errors.date}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <TextInput
                labelIcon="euro-sign"
                name="amount"
                prefix="paymentform"
                value={values.amount}
                touched={touched.amount}
                error={errors.amount}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <CheckboxInput
                label="paid"
                name="isPaid"
                prefix="paymentform"
                value={values.isPaid}
                touched={touched.isPaid}
                error={errors.isPaid}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <TextInput
                labelIcon="redo"
                name="period"
                prefix="paymentform"
                value={values.period}
                touched={touched.period}
                error={errors.period}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            {values.period && (
                <React.Fragment>
                    <TextInput
                        labelIcon="step-backward"
                        name="from"
                        prefix="paymentform"
                        value={values.from}
                        touched={touched.from}
                        error={errors.from}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />

                    <TextInput
                        labelIcon="step-forward"
                        name="to"
                        prefix="paymentform"
                        value={values.to}
                        touched={touched.to}
                        error={errors.to}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </React.Fragment>
            )}

            <div style={style.submit}>
                <IconSubmitButton
                    disabled={isSubmitting || !isValid}
                    tabIndex={0}
                />
            </div>
        </form>

        {payment && (
            <div style={style.delete}>
                <DeleteButton
                    onClick={() => {
                        onDelete();
                        onSuccess();
                    }}
                    tabIndex={0}
                />
            </div>
        )}
    </div>
);

const formFieldsPropType = (type = PropTypes.string) => PropTypes.shape({
    title: type,
    date: type,
    amount: type,
    isPaid: PropTypes.bool,

    period: type,
    from: type,
    to: type,
});

PaymentForm.propTypes = {
    payment: PaymentPropType,
    onSuccess: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,

    values: formFieldsPropType().isRequired,
    errors: formFieldsPropType().isRequired,
    touched: formFieldsPropType(PropTypes.bool).isRequired,

    handleChange: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,

    isSubmitting: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
};

PaymentForm.defaultProps = {
    payment: null,
};

export default WithServices(connect(
    state => ({
        date: categoriesDateSelector(state),
    }),
    (dispatch, { services, categoryId, payment }) => ({
        onSubmit: form => dispatch(savePayment(services, { ...form, categoryId }, payment)),
        onDelete: () => dispatch(removePayment(services, payment)),
    }),
)(withFormik({
    mapPropsToValues: ({ payment, date }) => {
        const values = {
            title: 'Neuer Eintrag',
            date: format(date, 'DD.MM.YYYY'),
            amount: '17.00',
            isPaid: false,
            period: '',
            from: '01.01.2018',
            to: '',
        };

        if (payment) {
            values.title = payment.title;
            values.date = format(payment.date, 'DD.MM.YYYY');
            values.amount = (payment.amount / 100.0).toFixed(2).toString();
            values.isPaid = payment.isPaid;

            if (payment.recurring) {
                values.period = payment.recurring.period.toString();
                values.from = format(payment.recurring.from, 'DD.MM.YYYY');
                values.to = payment.recurring.to ? format(payment.recurring.to, 'DD.MM.YYYY') : '';
            } else {
                values.from = values.date;
            }
        }
        return values;
    },
    validate: (values) => {
        const errors = {};
        if (!values.title) errors.title = 'Required';
        if (!values.date) errors.date = 'Required';
        if (!values.amount) errors.amount = 'Required';

        if (values.period) {
            const period = Number.parseInt(values.period, 10);
            if (period < 1 || period > 12) {
                errors.period = 'Invalid';
            } else if (!values.from) errors.from = 'Required';
        }

        return errors;
    },
    handleSubmit: (values, { props, setSubmitting }) => {
        const { onSubmit, onSuccess } = props;

        onSubmit(values);
        setSubmitting(false);
        onSuccess();
    },
})(PaymentForm)));
