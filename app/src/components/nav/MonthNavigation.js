import { format, subMonths, addMonths } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import {
    showCategories as showCategoriesAction,
    categoriesDateSelector,
    categoriesIsLoadingSelector,
} from 'redux/modules/categories';
import { resetList } from 'styles/reset.styles';

import NextLink from 'components/links/NextLink';
import StatisticsLink from './StatisticsLink';

const style = {
    container: {
        ...resetList,
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },

    label: {
        fontSize: 12,
        width: 120,
        textAlign: 'center',
        paddingTop: 2,
        paddingBottom: 2,
    },

    labelHeader: {
        fontSize: 20,
    },
};

const MonthNavigation = ({
    date, showCategories, isLoading,
}) => (
    <div style={style.container}>
        <NextLink
            to={format(subMonths(date, 1), '/YYYY/MM')}
            onClick={() => showCategories(format(subMonths(date, 1), 'YYYY-MM-DD'))}
            disabled={isLoading}
            flipped
        />
        <div style={style.label}>
            <StatisticsLink date={date}>
                {format(date, 'YYYY')}
            </StatisticsLink>
            <br />
            <span style={style.labelHeader}>{format(date, 'MMMM')}</span>
        </div>
        <NextLink
            to={format(addMonths(date, 1), '/YYYY/MM')}
            onClick={() => showCategories(format(addMonths(date, 1), 'YYYY-MM-DD'))}
            disabled={isLoading}
        />
    </div>
);

MonthNavigation.propTypes = {
    date: PropTypes.string,
    isLoading: PropTypes.bool.isRequired,

    showCategories: PropTypes.func.isRequired,
};

MonthNavigation.defaultProps = {
    date: null,
};

export default WithServices(connect(
    state => ({
        date: categoriesDateSelector(state),

        isLoading: categoriesIsLoadingSelector(state),
    }),
    (dispatch, { services }) => ({
        showCategories: date => dispatch(showCategoriesAction(services, date)),
    }),
)(MonthNavigation));
