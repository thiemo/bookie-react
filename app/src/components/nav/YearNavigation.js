import { format, subYears, addYears } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import {
    showStatistics as showStatisticsAction,
    statisticsDateSelector,
    statisticsIsLoadingSelector,
} from 'redux/modules/statistics';
import { resetList } from 'styles/reset.styles';

import NextLink from 'components/links/NextLink';

const style = {
    container: {
        ...resetList,
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },

    label: {
        fontSize: 20,
        width: 120,
        textAlign: 'center',
        paddingTop: 2,
        paddingBottom: 2,
    },

};

const YearNavigation = ({
    date, showStatistics, isLoading,
}) => (
    <div style={style.container}>
        <NextLink
            to={format(subYears(date, 1), '/YYYY')}
            onClick={() => showStatistics(format(subYears(date, 1), 'YYYY-MM-DD'))}
            disabled={isLoading}
            flipped
        />
        <div style={style.label}>{format(date, 'YYYY')}</div>
        <NextLink
            to={format(addYears(date, 1), '/YYYY')}
            onClick={() => showStatistics(format(addYears(date, 1), 'YYYY-MM-DD'))}
            disabled={isLoading}
        />
    </div>
);

YearNavigation.propTypes = {
    date: PropTypes.string,
    isLoading: PropTypes.bool.isRequired,

    showStatistics: PropTypes.func.isRequired,
};

YearNavigation.defaultProps = {
    date: null,
};

export default WithServices(connect(
    state => ({
        date: statisticsDateSelector(state),

        isLoading: statisticsIsLoadingSelector(state),
    }),
    (dispatch, { services }) => ({
        showStatistics: (from, to) => dispatch(showStatisticsAction(services, from, to)),
    }),
)(YearNavigation));
