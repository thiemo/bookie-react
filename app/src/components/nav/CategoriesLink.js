import { format } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import {
    showCategories as showCategoriesAction,
    categoriesIsLoadingSelector,
} from 'redux/modules/categories';

import Link from 'components/links/Link';

const CategoriesLink = ({
    date, children, showCategories, isLoading,
}) => (
    <Link
        to={format(date, '/YYYY/MM')}
        onClick={() => showCategories(date)}
        disabled={isLoading}
    >
        { children }
    </Link>
);

CategoriesLink.propTypes = {
    date: PropTypes.string,
    children: PropTypes.node.isRequired,
    isLoading: PropTypes.bool.isRequired,

    showCategories: PropTypes.func.isRequired,
};

CategoriesLink.defaultProps = {
    date: format(new Date(), 'YYYY-MM-DD'),
};

export default WithServices(connect(
    state => ({
        isLoading: categoriesIsLoadingSelector(state),
    }),
    (dispatch, { services }) => ({
        showCategories: date => dispatch(showCategoriesAction(services, date)),
    }),
)(CategoriesLink));
