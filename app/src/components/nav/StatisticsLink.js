import { format } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import {
    showStatistics as showStatisticsAction,
    statisticsIsLoadingSelector,
} from 'redux/modules/statistics';

import Link from 'components/links/Link';

const StatisticsLink = ({
    date, children, showStatistics, isLoading,
}) => (
    <Link
        to={format(date, '/YYYY')}
        onClick={() => showStatistics(date)}
        disabled={isLoading}
    >
        { children }
    </Link>
);

StatisticsLink.propTypes = {
    date: PropTypes.string,
    children: PropTypes.node.isRequired,
    isLoading: PropTypes.bool.isRequired,

    showStatistics: PropTypes.func.isRequired,
};

StatisticsLink.defaultProps = {
    date: format(new Date(), 'YYYY-MM-DD'),
};

export default WithServices(connect(
    state => ({
        isLoading: statisticsIsLoadingSelector(state),
    }),
    (dispatch, { services }) => ({
        showStatistics: date => dispatch(showStatisticsAction(services, date)),
    }),
)(StatisticsLink));
