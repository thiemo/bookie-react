import PropTypes from 'prop-types';
import React from 'react';

import { resetLink } from 'styles/reset.styles';
import colors from 'styles/colors.styles';

const style = {
    ...resetLink,
    color: colors.secondaryColor,
};

const Link = ({ to, onClick, children }) => (
    <a
        href={to}
        onClick={(e) => {
            e.preventDefault();
            onClick();
        }}
        style={style}
    >
        {children}
    </a>
);

Link.propTypes = {
    to: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
};

export default Link;
