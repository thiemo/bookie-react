import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Radium from 'radium';

import colors from 'styles/colors.styles';

import Link from './Link';

const style = {
    icon: disabled => ({
        fontSize: 24,
        paddingLeft: 10,
        paddingTop: 2,
        paddingRight: 10,
        paddingBottom: 2,
        color: disabled ? colors.secondaryMutedColor : colors.secondaryColor,
    }),
};

const NextLink = ({ disabled, flipped, ...props }) => (
    <Link {...props} disabled={disabled}>
        <FontAwesomeIcon
            icon="caret-right"
            style={style.icon(disabled)}
            flip={flipped ? 'horizontal' : null}
        />
    </Link>
);

NextLink.propTypes = {
    disabled: PropTypes.bool,
    flipped: PropTypes.bool,
};

NextLink.defaultProps = {
    disabled: false,
    flipped: false,
};

export default Radium(NextLink);
