import Radium from 'radium';
import React from 'react';

import colors from 'styles/colors.styles';
import logo from 'assets/logo.svg';

const style = {
    header: {
        textAlign: 'center',
        backgroundColor: colors.primaryColor,
        height: 150,
        padding: 20,
        color: colors.contrastColor,
    },

    logo: {
        height: 80,
        animation: 'x infinite 20s linear',
        animationName: Radium.keyframes({
            '0%': { transform: 'rotate(0deg)' },
            '100%': { transform: 'rotate(360deg)' },
        }, 'spin'),
    },

    title: {
        fontSize: '1.5em',
    },
};

const Header = () => (
    <header style={style.header}>
        <img src={logo} style={style.logo} alt="logo" />
        <h1 style={style.title}>Bookie 0.1.0</h1>
    </header>
);

export default Radium(Header);
