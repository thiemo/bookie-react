import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Radium from 'radium';

import { buttonBaseStyle as style } from 'styles/button.styles';

import BaseButton from './BaseButton';

const IconButton = ({ disabled, icon, ...props }) => (
    <div
        style={[
            style.container,
            disabled && style.containerDisabled,
        ]}
    >
        <BaseButton {...props} disabled={disabled}>
            <div
                style={[
                    style.submit,
                    disabled && style.submitDisabled,
                ]}
            >
                <FontAwesomeIcon icon={icon} style={style.icon} />
            </div>
        </BaseButton>
    </div>
);

IconButton.propTypes = {
    disabled: PropTypes.bool,
    icon: PropTypes.string,
};

IconButton.defaultProps = {
    disabled: false,
    icon: 'plus',
};

export default Radium(IconButton);
