import Radium from 'radium';
import React from 'react';

import { resetButton } from 'styles/reset.styles';

const BaseSubmitButton = props => (
    <button
        style={resetButton}
        type="submit"
        {...props}
    />
);

export default Radium(BaseSubmitButton);
