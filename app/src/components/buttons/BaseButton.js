import React from 'react';

import { resetButton } from 'styles/reset.styles';

const BaseButton = props => (
    <button
        style={resetButton}
        type="button"
        {...props}
    />
);

export default BaseButton;
