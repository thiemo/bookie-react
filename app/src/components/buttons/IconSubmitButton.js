import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Radium from 'radium';

import { buttonBaseStyle as style } from 'styles/button.styles';

import BaseSubmitButton from './BaseSubmitButton';

const IconSubmitButton = ({ disabled, icon, ...props }) => (
    <div
        style={[
            style.container,
            disabled && style.containerDisabled,
        ]}
    >
        <BaseSubmitButton {...props} disabled={disabled}>
            <div
                style={[
                    style.submit,
                    disabled && style.submitDisabled,
                ]}
            >
                <FontAwesomeIcon icon={icon} style={style.icon} />
            </div>
        </BaseSubmitButton>
    </div>
);

IconSubmitButton.propTypes = {
    disabled: PropTypes.bool,
    icon: PropTypes.string,
};

IconSubmitButton.defaultProps = {
    disabled: false,
    icon: 'plus',
};

export default Radium(IconSubmitButton);
