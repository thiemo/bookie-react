import React from 'react';

import IconButton from './IconButton';

const DeleteButton = props => (
    <IconButton {...props} icon="trash" />
);

export default DeleteButton;
