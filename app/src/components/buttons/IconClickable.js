import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

import { buttonIconStyle } from 'styles/button.styles';
import colors from 'styles/colors.styles';

import BaseButton from './BaseButton';

const style = {
    ...buttonIconStyle,
    padding: 10,
    color: colors.secondaryColor,
};

const IconClickable = ({ disabled, icon, ...props }) => (
    <BaseButton {...props} disabled={disabled}>
        <FontAwesomeIcon icon={icon} style={style} />
    </BaseButton>
);

IconClickable.propTypes = {
    disabled: PropTypes.bool,
    icon: PropTypes.string,
};

IconClickable.defaultProps = {
    disabled: false,
    icon: 'plus',
};

export default IconClickable;
