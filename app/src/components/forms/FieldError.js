import PropTypes from 'prop-types';
import React from 'react';

import colors from 'styles/colors.styles';

const style = {
    color: colors.primaryColor,
    marginBottom: 4,
};

const FieldError = ({ error, touched }) => {
    if (touched && error) {
        return (
            <div style={style}>
                {error}
            </div>
        );
    }
    return null;
};

FieldError.propTypes = {
    touched: PropTypes.bool,
    error: PropTypes.string,
};

FieldError.defaultProps = {
    touched: false,
    error: null,
};

export default FieldError;
