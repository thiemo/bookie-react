import PropTypes from 'prop-types';
import Radium from 'radium';
import React from 'react';

import colors from 'styles/colors.styles';

import FieldError from './FieldError';
import Label from './Label';

const style = {
    checkbox: {
        display: 'inline-block',
        position: 'relative',
        cursor: 'pointer',
        userSelect: 'none',
        width: 22,
        height: 22,
        lineHeight: '22px',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: colors.primaryColor,
        backgroundColor: colors.primaryAccentColor,

        ':focus': {
            borderColor: colors.contrastColor,
            outline: 'none',
        },
    },

    checkmark: {
        position: 'absolute',
        top: 0,
        left: 6,
        width: 7,
        height: 15,
        borderWidth: '0 3px 3px 0',
        borderStyle: 'solid',
        borderColor: colors.contrastColor,
        transform: 'rotate(40deg)',
    },

    input: {
        opacity: 0,
        left: 0,
        cursor: 'pointer',
    },
};

const CheckboxInput = ({
    label, name, value, prefix, onChange, onBlur, touched, error,
}) => (
    <React.Fragment>
        <FieldError touched={touched} error={error} />
        <Label htmlFor={`${prefix}-${name}`} label={label}>
            <div style={style.checkbox}>
                <input
                    id={`${prefix}-${name}`}
                    type="checkbox"
                    name={name}
                    onChange={onChange}
                    onBlur={onBlur}
                    value={value}
                    style={style.input}
                    tabIndex={0}
                />
                {value && <span style={style.checkmark} />}
            </div>
        </Label>
    </React.Fragment>
);

CheckboxInput.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.bool.isRequired,
    prefix: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    ...FieldError.propTypes,
};

export default Radium(CheckboxInput);
