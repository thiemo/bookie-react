import PropTypes from 'prop-types';
import Radium from 'radium';
import React from 'react';

import colors from 'styles/colors.styles';

import FieldError from './FieldError';
import Label from './Label';

const style = {
    width: 181,
    display: 'inline-block',
    boxSizing: 'border-box',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.primaryColor,
    backgroundColor: colors.primaryAccentColor,
    color: colors.contrastColor,
    fontSize: 18,
    lineHeight: '22px',
    paddingLeft: 8,
    paddingTop: 3,
    paddingRight: 8,
    paddingBottom: 5,

    ':focus': {
        borderColor: colors.contrastColor,
        outline: 'none',
    },
};

const TextInput = ({
    label, labelIcon, name, value, prefix, onChange, onBlur, touched, error,
}) => (
    <React.Fragment>
        <FieldError touched={touched} error={error} />
        <Label htmlFor={`${prefix}-${name}`} label={label} labelIcon={labelIcon}>
            <input
                id={`${prefix}-${name}`}
                type="text"
                name={name}
                onChange={onChange}
                onBlur={onBlur}
                value={value}
                style={style}
                tabIndex={0}
            />
        </Label>
    </React.Fragment>
);

TextInput.propTypes = {
    label: PropTypes.string,
    labelIcon: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    prefix: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    ...FieldError.propTypes,
};

TextInput.defaultProps = {
    label: null,
    labelIcon: null,
};

export default Radium(TextInput);
