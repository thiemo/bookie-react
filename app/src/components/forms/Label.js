import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';

import colors from 'styles/colors.styles';

const style = {
    label: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: 10,
    },

    icon: {
        fontSize: 20,
        marginRight: 8,
        flexGrow: 1,
        display: 'inline-block',
    },

    text: {
        color: colors.primaryColor,
        textTransform: 'uppercase',
        fontWeight: 'bold',
        display: 'inline-block',
        lineHeight: '22px',
        position: 'relative',
        marginRight: 10,
        flexGrow: 1,
    },

    input: {
        display: 'inline-block',
    },
};

const Label = ({
    htmlFor, label, labelIcon, children,
}) => (
    <label htmlFor={htmlFor} style={style.label}>
        {labelIcon && (
            <FontAwesomeIcon icon={labelIcon} style={style.icon} />
        )}
        {label && (
            <span style={style.text}>
                {label}
            </span>
        )}
        <div style={style.input}>
            {children}
        </div>
    </label>
);

Label.propTypes = {
    htmlFor: PropTypes.string.isRequired,
    label: PropTypes.string,
    labelIcon: PropTypes.string,
    children: PropTypes.node.isRequired,
};

Label.defaultProps = {
    label: null,
    labelIcon: null,
};

export default Label;
