import PropTypes from 'prop-types';
import Radium from 'radium';
import React from 'react';
import DatePicker from 'react-datepicker';

import colors from 'styles/colors.styles';

import FieldError from './FieldError';
import Label from './Label';

const style = {
    width: 181,
    display: 'inline-block',
    boxSizing: 'border-box',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.primaryColor,
    backgroundColor: colors.primaryAccentColor,
    color: colors.contrastColor,
    fontSize: 18,
    lineHeight: '22px',
    paddingLeft: 8,
    paddingTop: 3,
    paddingRight: 8,
    paddingBottom: 5,

    ':focus': {
        borderColor: colors.contrastColor,
        outline: 'none',
    },
};

const DateInput = ({
    label, labelIcon, name, value, prefix, onChange, onBlur, touched, error, setFieldValue,
}) => {
    const [day, month, year] = value.split('.');
    return (
        <React.Fragment>
            <FieldError touched={touched} error={error} />
            <Label htmlFor={`${prefix}-${name}-day`} label={label} labelIcon={labelIcon}>
                <input
                    id={`${prefix}-${name}-day`}
                    key="day"
                    type="number"
                    name={`${name}-day`}
                    onChange={({ target }) => { setFieldValue(name, `${target.value}-${month}-${year}`); }}
                    onBlur={onBlur}
                    value={day}
                    style={style}
                    tabIndex={0}
                />
                <input
                    id={`${prefix}-${name}-month`}
                    key="month"
                    type="number"
                    name={`${name}-month`}
                    onChange={({ target }) => { setFieldValue(name, `${day}-${target.value}-${year}`); }}
                    onBlur={onBlur}
                    value={month}
                    style={style}
                    tabIndex={0}
                />
                <input
                    id={`${prefix}-${name}-year`}
                    key="year"
                    type="number"
                    name={`${name}-year`}
                    onChange={({ target }) => { setFieldValue(name, `${day}-${month}-${target.value}`); }}
                    onBlur={onBlur}
                    value={year}
                    style={style}
                    tabIndex={0}
                />
                <input
                    id={`${prefix}-${name}`}
                    type="text"
                    name={`${name}`}
                    onChange={onChange}
                    onBlur={onBlur}
                    value={value}
                />
            </Label>
        </React.Fragment>);
};

DateInput.propTypes = {
    label: PropTypes.string,
    labelIcon: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    prefix: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    setFieldValue: PropTypes.func.isRequired,
    ...FieldError.propTypes,
};

DateInput.defaultProps = {
    label: null,
    labelIcon: null,
};

export default Radium(DateInput);
