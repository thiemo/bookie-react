import { format } from 'date-fns';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import colors from 'styles/colors.styles';
import { buttonIconStyle } from 'styles/button.styles';
import { logout } from 'redux/modules/auth';
import { showModal, CATEGORY_MODAL } from 'redux/modules/modal';

import IconClickable from 'components/buttons/IconClickable';
import CategoriesLink from 'components/nav/CategoriesLink';
import StatisticsLink from 'components/nav/StatisticsLink';

const style = {
    footer: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        right: 0,
        textAlign: 'center',
        paddingBottom: 6,
        paddingLeft: 8,
        backgroundColor: colors.primaryColor,
    },

    left: {
        display: 'inline-block',
        bottom: 6,
        left: 8,
        position: 'absolute',
    },

    right: {
        display: 'inline-block',
        bottom: 6,
        right: 8,
        position: 'absolute',
    },

    icon: {
        ...buttonIconStyle,
        padding: 10,
        color: colors.secondaryColor,
    },
};

const Footer = ({ onLogout, onAddCategory }) => (
    <div style={style.footer}>
        <div style={style.left}>
            <IconClickable onClick={onAddCategory} />
        </div>

        <StatisticsLink>
            <FontAwesomeIcon icon="bars" style={style.icon} />
        </StatisticsLink>

        <CategoriesLink>
            <FontAwesomeIcon icon="th-large" style={style.icon} />
        </CategoriesLink>

        {
            // <FontAwesomeIcon icon="chart-line" style={style.icon} />
        }

        <div style={style.right}>
            <IconClickable onClick={onLogout} icon="sign-out-alt" />
        </div>
    </div>
);

Footer.propTypes = {
    onLogout: PropTypes.func.isRequired,
    onAddCategory: PropTypes.func.isRequired,
};

export default connect(
    null,
    dispatch => ({
        onShowCategories: () => dispatch(push(format(new Date(), '/YYYY/MM'))),
        onShowStatistics: () => dispatch(push(format(new Date(), '/YYYY'))),
        onLogout: () => dispatch(logout()),

        onAddCategory: () => dispatch(showModal(CATEGORY_MODAL)),
    }),
)(Footer);
