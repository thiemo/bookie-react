import { format } from 'date-fns';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Masonry from 'react-masonry-component';

import WithServices from 'api/ServicesProvider';
import {
    showCategories as showCategoriesAction,
    CategoryPropType,
    categoriesSelector,
    categoriesIsLoadingSelector,
} from 'redux/modules/categories';

import MonthNavigation from 'components/nav/MonthNavigation';
import Category from './Category';

const style = {
    item: {
        padding: 10,
        boxSizing: 'border-box',
        width: '33%',
    },
};

class CategoryList extends React.Component {
    static propTypes = {
        categories: PropTypes.arrayOf(CategoryPropType),
        isLoading: PropTypes.bool.isRequired,
        date: PropTypes.string.isRequired,

        showCategories: PropTypes.func.isRequired,
    };

    static defaultProps = {
        categories: null,
    }

    componentDidMount() {
        const {
            categories, isLoading, showCategories, date,
        } = this.props;
        if (categories === null && !isLoading) {
            showCategories(format(date, 'YYYY-MM-DD'));
        }
    }

    render() {
        const { categories } = this.props;

        return (
            <React.Fragment>
                <MonthNavigation />

                <Masonry>
                    {categories && categories.map(category => (
                        <div key={category._id} style={style.item}>
                            <Category category={category} />
                        </div>))}
                </Masonry>

            </React.Fragment>
        );
    }
}

export default WithServices(connect(
    state => ({
        categories: categoriesSelector(state),

        isLoading: categoriesIsLoadingSelector(state),
    }),
    (dispatch, { services }) => ({
        showCategories: date => dispatch(showCategoriesAction(services, date)),
    }),
)(CategoryList));
