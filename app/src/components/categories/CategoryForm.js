import { withFormik } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import WithServices from 'api/ServicesProvider';
import { buttonSpace } from 'styles/button.styles';
import { alignBottomRight, alignBottomLeft } from 'styles/common.styles';
import {
    saveCategory,
    removeCategory,
    CategoryPropType,
} from 'redux/modules/categories';

import IconSubmitButton from 'components/buttons/IconSubmitButton';
import DeleteButton from 'components/buttons/DeleteButton';
import TextInput from 'components/forms/TextInput';

const style = {
    container: {
        position: 'relative',
    },
    form: {
        ...buttonSpace,
        display: 'inline-block',
    },
    submit: alignBottomRight,
    delete: alignBottomLeft,
};

const CategoryForm = ({
    category,
    onDelete,
    onSuccess,
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    touched,
    errors,
    isSubmitting,
    isValid,
}) => (
    <div style={style.container}>
        <form onSubmit={handleSubmit} style={style.form}>
            <TextInput
                labelIcon="caret-right"
                name="title"
                prefix="categoryform"
                value={values.title}
                touched={touched.title}
                error={errors.title}
                onChange={handleChange}
                onBlur={handleBlur}
            />

            <div style={style.submit}>
                <IconSubmitButton
                    disabled={isSubmitting || !isValid}
                    tabIndex={0}
                />
            </div>
        </form>

        {category && (
            <div style={style.delete}>
                <DeleteButton
                    onClick={() => {
                        onDelete();
                        onSuccess();
                    }}
                    tabIndex={0}
                />
            </div>
        )}
    </div>
);

const formFieldsPropType = (type = PropTypes.string) => PropTypes.shape({
    title: type,
});

CategoryForm.propTypes = {
    category: CategoryPropType,
    onSuccess: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,

    values: formFieldsPropType().isRequired,
    errors: formFieldsPropType().isRequired,
    touched: formFieldsPropType(PropTypes.bool).isRequired,

    handleChange: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,

    isSubmitting: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
};

CategoryForm.defaultProps = {
    category: null,
};

export default WithServices(connect(
    null,
    (dispatch, { services, category }) => ({
        onSubmit: form => dispatch(saveCategory(services, form, category)),
        onDelete: () => dispatch(removeCategory(services, category)),
    }),
)(withFormik({
    mapPropsToValues: ({ category }) => {
        const values = {
            title: 'New Category',
        };

        if (category) {
            values.title = category.title;
        }
        return values;
    },
    validate: (values) => {
        const errors = {};
        if (!values.title) errors.title = 'Required';
        return errors;
    },
    handleSubmit: (values, { props, setSubmitting }) => {
        const { onSubmit, onSuccess } = props;

        onSubmit(values);
        setSubmitting(false);
        onSuccess();
    },
})(CategoryForm)));
