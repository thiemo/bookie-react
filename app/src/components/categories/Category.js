import PropTypes from 'prop-types';
import React from 'react';
import ReactTable from 'react-table';
import { format } from 'date-fns';
import { connect } from 'react-redux';

import { formatCents } from 'utils';
import colors from 'styles/colors.styles';
import { CategoryPropType } from 'redux/modules/categories';
import { showModal, PAYMENT_MODAL, CATEGORY_MODAL } from 'redux/modules/modal';

import BaseButton from 'components/buttons/BaseButton';
import IconClickable from 'components/buttons/IconClickable';

const borderColor = colors.secondaryMutedColor;
const style = {
    container: {
        position: 'relative',
    },

    title: {
        fontSize: 20,
        color: colors.contrastColor,
    },

    statistics: {
        position: 'absolute',
        right: 0,
        top: 0,
    },

    paid: {
        // fontWeight: 'bold',
        fontSize: 18,
        color: colors.secondaryAccentColor,
    },

    missing: {
        color: colors.secondaryMutedColor,
        fontSize: 14,
        position: 'relative',
        // top: -2,
    },

    delimiter: {
        color: colors.secondaryMutedColor,
        fontSize: 14,
        marginLeft: 4,
        marginRight: 4,
        display: 'inline-block',
    },

    table: {
        borderColor,
        backgroundColor: colors.primaryAccentColor,
    },

    thead: {
        boxShadow: 'none',
        backgroundColor: colors.secondaryColor,
        color: colors.primaryColor,
    },

    tr: {
        borderColor,
        cursor: 'pointer',
    },

    th: {
        borderColor,
        display: 'none',
    },

    td: ({ isPaid }, { id }) => {
        let textAlign = 'left';
        if (id === 'date') textAlign = 'center';
        else if (id === 'amount') textAlign = 'right';
        return {
            borderColor,
            textAlign,
            color: isPaid ? colors.secondaryAccentColor : colors.contrastMutedColor,
        };
    },

    add: {
        textAlign: 'right',
    },
};

const Category = ({
    category: { title, payments, statistic: { paid, total } }, onAdd, onEdit, onEditCategory,
}) => (
    <div style={style.container}>
        <BaseButton onClick={onEditCategory}>
            <span style={style.title}>{title}</span>
        </BaseButton>
        <div style={style.statistics}>
            <span style={style.paid}>{formatCents(paid)}</span>
            {paid !== total && (
                <React.Fragment>
                    <span style={style.delimiter}>/</span>
                    <span style={style.missing}>{formatCents(total - paid)}</span>
                </React.Fragment>
            )}
        </div>
        {payments.length > 0 && (
            <ReactTable
                data={payments}
                columns={[
                    {
                        Header: 'Date',
                        accessor: 'date',
                        Cell: ({ value }) => format(value, 'DD.MM.YYYY'),
                        minWidth: 94,
                    }, {
                        Header: 'Title',
                        accessor: 'title',
                    }, {
                        Header: 'Amount',
                        accessor: 'amount',

                        Cell: ({ value }) => formatCents(value),
                        minWidth: 90,
                    }, {
                        Header: 'Paid',
                        accessor: 'isPaid',
                        show: false,
                    },
                ]}
                showPagination={false}
                minRows={1}
                defaultSorted={[{ id: 'date' }, { id: 'title' }]}
                getProps={() => ({ style: style.table })}
                getTheadProps={() => ({ style: style.thead })}
                getTheadThProps={() => ({ style: style.th })}
                getTrGroupProps={(state, { original }) => ({
                    onClick: () => onEdit(original),
                    style: style.tr,
                })}
                getTdProps={(state, { row }, column) => ({ style: style.td(row, column) })}
            />
        )}

        <div style={style.add}>
            <IconClickable onClick={onAdd} />
        </div>
    </div>
);

Category.propTypes = {
    category: CategoryPropType.isRequired,
    onAdd: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onEditCategory: PropTypes.func.isRequired,
};

export default connect(
    null,
    (dispatch, { category }) => ({
        onAdd: () => dispatch(showModal(PAYMENT_MODAL, { categoryId: category._id })),
        onEdit: payment => dispatch(
            showModal(PAYMENT_MODAL, { payment, categoryId: category._id }),
        ),
        onEditCategory: () => dispatch(showModal(CATEGORY_MODAL, { category })),
    }),
)(Category);
