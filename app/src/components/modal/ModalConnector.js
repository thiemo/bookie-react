import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import {
    hideModal as hideModalAction,
    CATEGORY_MODAL,
    PAYMENT_MODAL,
    modalTypeSelector,
    modalPropsSelector,
} from 'redux/modules/modal';

import PaymentForm from 'components/payments/PaymentForm';
import CategoryForm from 'components/categories/CategoryForm';
import Modal from './Modal';

const ModalConnector = ({ modalType, modalProps, hideModal }) => {
    switch (modalType) {
    case CATEGORY_MODAL:
        return (
            <Modal hideModal={hideModal}>
                <CategoryForm {...modalProps} onSuccess={hideModal} />
            </Modal>
        );
    case PAYMENT_MODAL:
        return (
            <Modal hideModal={hideModal}>
                <PaymentForm {...modalProps} onSuccess={hideModal} />
            </Modal>
        );
    default:
        return null;
    }
};

ModalConnector.propTypes = {
    hideModal: PropTypes.func.isRequired,

    modalType: PropTypes.string,
    modalProps: PropTypes.shape({}),
};

ModalConnector.defaultProps = {
    modalType: null,
    modalProps: null,
};

export default connect(
    state => ({
        modalType: modalTypeSelector(state),
        modalProps: modalPropsSelector(state),
    }),
    dispatch => ({
        hideModal: () => dispatch(hideModalAction()),
    }),
)(ModalConnector);
