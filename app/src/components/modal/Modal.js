import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import ModalReact from 'react-modal';

import colors from 'styles/colors.styles';

import BaseButton from 'components/buttons/BaseButton';

const style = {
    modal: {
        overlay: {
            zIndex: 9000,
            backgroundColor: 'rgba(0,0,0,0.1)',
        },
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            borderColor: colors.primaryAccentColor,
            borderRadius: 0,
            backgroundColor: colors.secondaryColor,
            color: colors.primaryColor,
            padding: 25,
        },
    },

    close: {
        position: 'absolute',
        right: 0,
        top: 0,
    },

    icon: {
        fontSize: 18,
        paddingTop: 3,
        paddingRight: 5,
        paddingBottom: 3,
        paddingLeft: 5,
        color: colors.primaryColor,
    },
};

const Modal = ({ children, hideModal }) => (
    <ModalReact
        isOpen
        onRequestClose={hideModal}
        style={style.modal}
    >
        <div style={style.close}>
            <BaseButton onClick={hideModal}>
                <FontAwesomeIcon icon="times" style={style.icon} />
            </BaseButton>
        </div>
        {children}
    </ModalReact>
);

Modal.propTypes = {
    hideModal: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
};

export default Modal;
