import auth from './modules/auth';
import categories from './modules/categories';
import modal from './modules/modal';
import payments from './modules/payments';
import statistics from './modules/statistics';

export default services => ({
    categoriesAPI: services.categories.reducer,
    paymentsAPI: services.payments.reducer,
    paymentsRecurringAPI: services.paymentsRecurring.reducer,
    statisticsAPI: services.statistics.reducer,
    auth,
    categories,
    modal,
    payments,
    statistics,
});
