import {
    createStore,
    applyMiddleware,
    combineReducers,
    compose,
} from 'redux';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import reduxThunk from 'redux-thunk';
import reduxPromise from 'redux-promise-middleware';

import createReducers from './reducer';

const middlewares = [];
let composeEnhancers = compose;
if (process.env.NODE_ENV === 'development') {
    const { createLogger } = require('redux-logger'); // eslint-disable-line global-require
    middlewares.push(createLogger({
        collapsed: true,
    }));
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

export default function configureStore(services, preloadedState) {
    const history = createBrowserHistory();

    const store = createStore(
        connectRouter(history)(combineReducers(createReducers(services))),
        preloadedState,
        composeEnhancers(
            applyMiddleware(
                // Lets us dispatch() functions
                reduxThunk,

                // Dispatch a promise as the value of the payload property of the action
                reduxPromise(),

                // Lets us dispatch() history actions
                routerMiddleware(history),

                ...middlewares,
            ),
        ),
    );

    return { store, history };
}
