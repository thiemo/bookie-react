/*
 * action types
 */

export const SHOW_MODAL = 'bookie.modal.SHOW_MODAL';
export const HIDE_MODAL = 'bookie.modal.HIDE_MODAL';

/*
 * action creators
 */

export const showModal = (modal, props) => ({
    type: SHOW_MODAL,
    modal,
    props,
});

export const hideModal = () => ({
    type: HIDE_MODAL,
});
