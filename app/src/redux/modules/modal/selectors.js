
export const modalTypeSelector = state => state.modal.type;

export const modalPropsSelector = state => state.modal.props;
