import { SHOW_MODAL, HIDE_MODAL } from './actions';

/*
 * reducers
 */

export default function reducer(state = {
    type: null,
    props: null,
}, action) {
    switch (action.type) {
    case SHOW_MODAL:
        return {
            type: action.modal,
            props: action.props,
        };
    case HIDE_MODAL:
        return {
            type: null,
            props: null,
        };
    default:
        return state;
    }
}
