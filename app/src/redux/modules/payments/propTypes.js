import PropTypes from 'prop-types';

export const RecurringPaymentPropType = PropTypes.shape({
    categoryId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    from: PropTypes.string.isRequired,
    to: PropTypes.string,
    period: PropTypes.number.isRequired,
});

export const PaymentPropType = PropTypes.shape({
    _id: PropTypes.string,
    tempId: PropTypes.string,
    categoryId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    isPaid: PropTypes.bool.isRequired,
    recurring: RecurringPaymentPropType,
});
