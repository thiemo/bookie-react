import {
    findCategories,
    showCategories,
    categoriesDateSelector,
} from 'redux/modules/categories';

/*
 * action types
 */


/*
 * action creators
 */

export const updatePayment = (services, payment, existing) => {
    if (existing) {
        if (existing._id) {
            return dispatch => dispatch(services.payments.patch(existing._id, payment));
        }
        if (existing.tempId) {
            const recurringId = existing.recurring ? existing.recurring._id : '';
            return dispatch => dispatch(services.payments.create({ ...payment, recurringId }));
        }
    }
    return dispatch => dispatch(services.payments.create(payment));
};

export const savePayment = (services, payment, existing) => (
    async (dispatch, getState) => {
        await dispatch(updatePayment(services, payment, existing));
        return dispatch(showCategories(services, categoriesDateSelector(getState())));
    }
);

export const removePayment = (services, existing) => (
    async (dispatch, getState) => {
        await (existing._id
            ? dispatch(services.payments.remove(existing._id))
            : dispatch(services.paymentsRecurring.remove(existing.recurring._id)));
        return dispatch(findCategories(services, categoriesDateSelector(getState())));
    }
);
