import { format, startOfYear, endOfYear } from 'date-fns';
import { push } from 'connected-react-router';

/*
 * action types
 */

export const SERVICES_STATISTICS_FIND_PENDING = 'SERVICES_STATISTICS_FIND_PENDING';
export const SERVICES_STATISTICS_FIND_FULFILLED = 'SERVICES_STATISTICS_FIND_FULFILLED';

export const SET_STATISTICS_TIMEFRAME = 'bookie.statistics.SET_STATISTICS_TIMEFRAME';

/*
 * action creators
 */

const setTimeframe = date => ({
    type: SET_STATISTICS_TIMEFRAME,
    date,
});

export const findStatistics = (services, date) => (
    dispatch => dispatch(services.statistics.find({
        query: {
            date: {
                $gte: format(startOfYear(date), 'YYYY-MM-DD'),
                $lte: format(endOfYear(date), 'YYYY-MM-DD'),
            },
        },
    }))
);

export const showStatistics = (services, date) => (
    async (dispatch, getState) => {
        await dispatch(setTimeframe(date));
        await dispatch(findStatistics(services, date));

        const path = format(date, '/YYYY');
        if (getState().router.location.pathname !== path) {
            await dispatch(push(path));
        }
    }
);
