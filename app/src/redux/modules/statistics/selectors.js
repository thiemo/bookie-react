import { createSelector } from 'reselect';

export const statisticsIsLoadingSelector = state => state.statistics.isLoading;

export const statisticsDateSelector = state => state.statistics.date;

export const statisticsSelector = state => state.statistics.statistics;

export const statisticsPerMonthSelector = createSelector(
    statisticsSelector,
    statistics => ((statistics)
        ? statistics.reduce((acc, statistic) => {
            const { categoryId, date } = statistic;
            let month = acc.find(m => m._date === date);
            if (!month) {
                month = { _date: date, _total: 0, _paid: 0, _missing: 0 };
                acc.push(month);
            }
            month[categoryId] = statistic;
            month._total += statistic.total;
            month._paid += statistic.paid;
            month._missing = month._total - month._paid;
            return acc;
        }, []).sort(({ _date: a }, { _date: b }) => new Date(a) - new Date(b)) : null
    ),
);
