
import {
    SERVICES_STATISTICS_FIND_PENDING,
    SERVICES_STATISTICS_FIND_FULFILLED,
    SET_STATISTICS_TIMEFRAME,
} from './actions';

/*
 * reducers
 */

export default function reducer(state = {
    date: null,
    statistics: null,

    isLoading: false,
}, action) {
    switch (action.type) {
    case SET_STATISTICS_TIMEFRAME:
        return {
            ...state,
            date: action.date,
        };
    case SERVICES_STATISTICS_FIND_PENDING:
        return {
            ...state,
            isLoading: true,
        };
    case SERVICES_STATISTICS_FIND_FULFILLED:
        return {
            ...state,
            statistics: action.payload.data,
            isLoading: false,
        };
    default:
        return state;
    }
}
