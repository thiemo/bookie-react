import PropTypes from 'prop-types';

export const StatisticPropType = PropTypes.shape({
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    paid: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
});

export const StatisticSlimmedPropType = PropTypes.shape({
    _id: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    paid: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
});
