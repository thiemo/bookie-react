
import {
    SERVICES_CATEGORIES_FIND_PENDING,
    SERVICES_CATEGORIES_FIND_FULFILLED,
    SET_CATEGORIES_TIMEFRAME,
} from './actions';

/*
 * reducers
 */

export default function reducer(state = {
    date: null,
    categories: null,

    isLoading: false,
}, action) {
    switch (action.type) {
    case SET_CATEGORIES_TIMEFRAME:
        return {
            ...state,
            date: action.date,
        };
    case SERVICES_CATEGORIES_FIND_PENDING:
        return {
            ...state,
            isLoading: true,
        };
    case SERVICES_CATEGORIES_FIND_FULFILLED:
        return {
            ...state,
            categories: action.payload.data.sort((a, b) => a.title.localeCompare(b.title)),
            isLoading: false,
        };
    default:
        return state;
    }
}
