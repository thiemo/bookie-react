import { format } from 'date-fns';
import { paramsForServer } from 'feathers-hooks-common';
import { push } from 'connected-react-router';

import { categoriesDateSelector } from './selectors';

/*
 * action types
 */

export const SERVICES_CATEGORIES_FIND_PENDING = 'SERVICES_CATEGORIES_FIND_PENDING';
export const SERVICES_CATEGORIES_FIND_FULFILLED = 'SERVICES_CATEGORIES_FIND_FULFILLED';

export const SET_CATEGORIES_TIMEFRAME = 'bookie.categories.SET_CATEGORIES_TIMEFRAME';

/*
 * action creators
 */

const setTimeframe = date => ({
    type: SET_CATEGORIES_TIMEFRAME,
    date,
});

export const findCategories = (services, date) => (
    dispatch => dispatch(services.categories.find(paramsForServer({
        resolveForMonth: format(date, 'YYYY-MM-DD'),
    })))
);

export const showCategories = (services, date) => (
    async (dispatch, getState) => {
        await dispatch(setTimeframe(date));
        await dispatch(findCategories(services, date));

        const path = format(date, '/YYYY/MM');
        if (getState().router.location.pathname !== path) {
            await dispatch(push(path));
        }
    }
);

export const updateCategory = (services, category, existing) => (
    dispatch => (existing
        ? dispatch(services.categories.patch(existing._id, category))
        : dispatch(services.categories.create(category)))
);

export const saveCategory = (services, category, existing) => (
    async (dispatch, getState) => {
        await dispatch(updateCategory(services, category, existing));
        return dispatch(showCategories(services, categoriesDateSelector(getState())));
    }
);

export const removeCategory = (services, existing) => (
    async (dispatch, getState) => {
        await dispatch(services.categories.remove(existing._id));
        return dispatch(findCategories(services, categoriesDateSelector(getState())));
    }
);
