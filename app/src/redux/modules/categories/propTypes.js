import PropTypes from 'prop-types';

import { StatisticSlimmedPropType } from 'redux/modules/statistics';
import { PaymentPropType } from 'redux/modules/payments';

export const CategoryPropType = PropTypes.shape({
    title: PropTypes.string.isRequired,
    statistic: StatisticSlimmedPropType.isRequired,
    payments: PropTypes.arrayOf(PaymentPropType).isRequired,
});
