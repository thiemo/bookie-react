
export const categoriesIsLoadingSelector = state => state.categories.isLoading;

export const categoriesDateSelector = state => state.categories.date;

export const categoriesSelector = state => state.categories.categories;
