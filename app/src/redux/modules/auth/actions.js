/*
 * action types
 */

export const ON_LOGIN_SUCCESS = 'bookie.auth.ON_LOGIN_SUCCESS';
export const ON_LOGOUT = 'bookie.auth.ON_LOGOUT';

/*
 * action creators
 */

export const login = user => ({
    type: ON_LOGIN_SUCCESS,
    user,
});

export const logout = () => ({
    type: ON_LOGOUT,
});
