import PropTypes from 'prop-types';

export const UserPropType = PropTypes.shape({
    _id: PropTypes.string.isRequired,
});

export default UserPropType;
