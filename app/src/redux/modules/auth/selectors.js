
export const userSelector = state => state.auth.user;

export default userSelector;
