import {
    ON_LOGIN_SUCCESS,
    ON_LOGOUT,
} from './actions';

/*
 * reducers
 */

export default function reducer(state = {
    user: null,
}, action) {
    switch (action.type) {
    case ON_LOGIN_SUCCESS:
        return {
            ...state,
            user: action.user,
        };
    case ON_LOGOUT:
        return {
            ...state,
            user: null,
        };
    default:
        return state;
    }
}
