import { StyleRoot } from 'radium';
import { Provider } from 'react-redux';
import React from 'react';
import Modal from 'react-modal';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import 'styles/global.css';
import 'styles/fontawesome.lib';
import 'react-table/react-table.css';

import configureStore from 'redux/configureStore';
import { configureServices, configureRealtime } from 'api';
import { ServicesProvider } from 'api/ServicesProvider';
import { login } from 'redux/modules/auth';

import PrivateRoute from 'pages/PrivateRoute';
import Error404Page from 'pages/Error404Page';
import LoginPage from 'pages/LoginPage';
import MonthOverviewPage from 'pages/MonthOverviewPage';
import YearOverviewPage from 'pages/YearOverviewPage';
import ModalConnector from 'components/modal/ModalConnector';

const { api, services } = configureServices();
const { store, history } = configureStore(services);
configureRealtime(api, services, store);

if (process.env.NODE_ENV === 'development') {
    api.authenticate({
        strategy: 'local',
        email: 'user@test.mail',
        password: 'password',
    }).then((response) => {
        store.dispatch(login(response.user));
    }).catch(e => console.log(e));
}

Modal.setAppElement('body');

export default () => (
    <Provider store={store}>
        <ServicesProvider api={api} services={services}>
            <StyleRoot>
                <React.Fragment>
                    <ConnectedRouter history={history}>
                        <Switch>
                            <Route path="/login" component={LoginPage} />
                            <PrivateRoute exact path="/:year([\d]{4})" component={YearOverviewPage} />
                            <PrivateRoute
                                exact
                                path="/:year([\d]{4})/:month([\d]{2})"
                                component={MonthOverviewPage}
                            />
                            <PrivateRoute exact path="/" component={MonthOverviewPage} />
                            <Route component={Error404Page} />
                        </Switch>
                    </ConnectedRouter>
                    <ModalConnector />
                </React.Fragment>
            </StyleRoot>
        </ServicesProvider>
    </Provider>
);
