
const colors = {
    primaryColor: 'rgb(32, 40, 51)',
    primaryAccentColor: 'rgb(51, 59, 68)',
    primaryMutedColor: 'rgb(23, 27, 33)',
    secondaryColor: 'rgb(119, 140, 163)',
    secondaryAccentColor: 'rgb(165, 177, 194)',
    secondaryMutedColor: 'rgb(96, 102, 112)',
    contrastColor: 'rgb(255, 255, 255)',
    contrastMutedColor: 'rgb(110, 110, 110)',
};

export default colors;
