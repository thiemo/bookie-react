
export const alignBottomLeft = {
    position: 'absolute',
    left: 0,
    bottom: 0,
};

export const alignBottomRight = {
    position: 'absolute',
    right: 0,
    bottom: 0,
};

export default alignBottomRight;
