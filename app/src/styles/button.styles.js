import colors from './colors.styles';

export const buttonSpace = {
    paddingBottom: 40,
    position: 'relative',
};

export const buttonStyle = {
    paddingTop: 7,
    paddingRight: 30,
    paddingBottom: 7,
    paddingLeft: 30,
};

export const buttonIconStyle = {
    fontSize: 16,
};

export const buttonBaseStyle = {
    container: {
        display: 'inline-block',
        boxSizing: 'border-box',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: colors.primaryColor,
        ':focus': {
            borderColor: colors.contrastColor,
        },
    },

    containerDisabled: {
        borderColor: colors.secondaryAccentColor,
    },

    submit: {
        ...buttonStyle,
        backgroundColor: colors.primaryAccentColor,
    },

    submitDisabled: {
        backgroundColor: colors.secondaryAccentColor,
    },

    icon: {
        ...buttonIconStyle,
        color: colors.contrastColor,
    },
};
