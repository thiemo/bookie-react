import colors from './colors.styles';

export const resetBoxModel = {
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    boxSizing: 'border-box',
};

export const resetList = {
    ...resetBoxModel,
    listStyle: 'none',
};

export const resetButton = {
    ...resetBoxModel,
    background: 'none',
    borderWidth: 0,
    cursor: 'pointer',
    outline: 'none',
    boxSizing: 'border-box',
    lineHeight: 'normal',
};

export const resetLink = {
    textDecoration: 'none',
    color: colors.secondaryColor,
};
