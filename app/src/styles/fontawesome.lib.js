import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faPlus,
    faTrash,
    faPen,
    faRedo,
    faCheck,
    faTimes,
    faSearch,
    faSignInAlt,
    faSignOutAlt,
    faCaretRight,
    faCalendarAlt,
    faThLarge,
    faBars,
    faChartLine,
    faStepBackward,
    faStepForward,
    faDollarSign,
    faEuroSign,
} from '@fortawesome/free-solid-svg-icons';

library.add(
    faPlus, faTrash, faPen, faRedo, faCheck, faTimes, faSearch, faSignInAlt, faSignOutAlt,
    faCaretRight, faCalendarAlt, faThLarge, faBars, faChartLine, faStepBackward, faStepForward,
    faDollarSign, faEuroSign,
);
