## TODO 

# App
- responsive
- reduce index files
- payment form
    + validation with Yup
    + currency delimiter
    + notification when saved/created payment that lies in another month and therefore is not displayed
    + modal also closes on api error -> fix

# api
- logging (morgan)
- fastJoin statisticsMonth

# deployment
- analyze bundle size: https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#analyzing-the-bundle-size